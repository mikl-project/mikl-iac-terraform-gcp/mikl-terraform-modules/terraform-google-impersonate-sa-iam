resource "google_service_account" "impersonate-service-account" {
  project = var.project
  account_id   = var.account_id
  display_name = var.display_name
  create_ignore_already_exists = true


  lifecycle {
      prevent_destroy = true
     
    #  ignore_changes = [tags]
     }
}

resource "google_project_iam_binding" "iam-roles" {
   
    project = var.project
    members = [
    var.type == "serviceAccount" ? "serviceAccount:${google_service_account.impersonate-service-account.email}" : var.type,
  ]
#    members = ["var.type:${google_service_account.mirakl-service-account.email}"]


    for_each = toset(var.role)
    role = each.key


}

# no depends on and argument usage not needed in another module why because this is the first step it self
